# GIT SUMMARY


**GIT** 

 An open Source,distributed version-control system.

 **Terminology**


 **Commit** - When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

 **Push** -  A push is  forcing our commits into the target repository, and a **push request** is the target repository requesting you to push our commits.
 
 **Pull** - A pull is the target repository grabbing our commits to be present there, and a **pull request** is  requesting the target repository to pull our commits.

 **Branch** - Git repository is viewed as tree. The trunk of the tree, the main software, is called the **Master Branch**. The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

 **Merge** - Integrating two branches together (when branch is free of bugs).

 **Clone** - Downloading  an existing Git repository to  local computer.

 **Fork** - got an entirely new repository of that code under our own name.

 
 # Git Workflow

 **The GitLab workflow can be condensed into:**


**Branching** → **Commits** → **Pull Requests** → **Collaboration** → **Merge**

![Github Workflow](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

 

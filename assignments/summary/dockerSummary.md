# Dockers Summary

**Docker** is a container management service. Docker is an open platform for developing, shipping, and running applications. 

## Terminology

**Docker Image** - An image is a read-only template with instructions for creating a Docker container. It contains everything needed to run applications as a container. This includes:
* code
* runtime
* libraries
* environment variables
* configuration files

The image can then be deployed to any Docker environment and as a container.


**Container** - A container is a runnable instance of an image. We can create, start, stop, move, or delete a container using the Docker API or CLI.

**DockersHub** - Docker Hub is like GitHub but for docker images and containers.

## Docker Terminology

### dockerps
 Allows us to view all the containers that are running on the Docker Host.

### docker start
Starts one or more stopped containers.

### docker stop
Stops one or more running containers.

### docker run
Runs a command in a new container.

### docker rm
Delete the containers.

## Docker Workflow
![Step-by-step workflow for developing Docker containerized apps](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)
